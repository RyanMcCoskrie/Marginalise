class String
	def marginalise(m)
		lines = [""]
		self.split(' ').each do |word|
			if (lines.last.size + word.size + 1) < m
				if lines.last == ""
					lines[lines.size-1] = word
				else
					lines[lines.size-1] = lines.last+" "+word
				end
			else
				lines << word
			end
		end
		return lines.join "\n"
	end
end
