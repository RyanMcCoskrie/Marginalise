Gem::Specification.new do |s|
	s.name		=					'marginalise'
	s.version	=						'0.1'
	s.date		=					 '2018-05-31'
	s.summary	=					'Marginalise'
	s.description	=		'Reformats strings to a given margin'
	s.authors	=				     'Ryan McCoskrie'
	s.email		=			      'work@ryanmccoskrie.me'
	s.files		=				['lib/marginalise.rb']
	s.homepage	=	'http://gitlab.com/RyanMcCoskrie/Marginalise'
	s.license	=	'MIT'
end
